# Happiness Trap Sidekick

A simple, unofficial, website to help you along when reading 'The Happiness Trap' by Dr Russ Harris. ([http://www.thehappinesstrap.com/](http://www.thehappinesstrap.com/)).
This site is not affiliated with or endorsed by Dr Russ Harris. It was built by me for two reasons:

1. Make it easier to do the activities in the book as I read it
2. Test out elixir + elm combo with gitlab's CI tool

## Inspiration

- [The colours on this design](https://www.behance.net/gallery/41910087/Essential-free-icons)

## Develop Prereq

 - [Elm](http://elm-lang.org/install)

## To Run Locally

1. `elm-package install`
2. `elm-reactor`
3. Go to [http://localhost:8000/src/index-local.html](http://localhost:8000/src/index-local.html)

## To Build

In terminal:

1. `elm-package install`
2. `elm-make src/Main.elm --output dist/app.js`
3. `cp src/index.html .public/index.html`
4. `cp src/styles.css .public/styles.css`
