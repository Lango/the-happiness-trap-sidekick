import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Http
import List.Extra as ListExtra
import Json.Decode as Decode
import Json.Decode.Pipeline as Pipeline exposing (required, decode)
import Task

main =
    App.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


-- MODEL


model : Model
model = questionnaireData

type alias Model =
    { title : String
    , description : String
    , reference : String
    , copyright : String
    , answeringState : AnsweringState
    }


type AnsweringState
    = ReadyToStart
    | Loading
    | LoadError Http.Error
    | AnsweringQuestions AnsweringQuestionsModel
    | Finished (List AnsweredQuestion)
    | NoQuestionsProvided

type alias UnansweredQuestion =
    List Choice

type alias AnsweringQuestionsModel =
    { answeredQuestions : List AnsweredQuestion
    , currentQuestion : UnansweredQuestion
    , unansweredQuestions : List UnansweredQuestion
    }

type alias AnsweredQuestion =
    { beforeChosen : List Choice
    , chosen : Choice
    , afterChosen : List Choice
    }

type alias Choice =
    { choiceId : Int
    , questionNumber : Int
    , description : String
    , choiceType : ChoiceType
    }

type ChoiceType
    = A
    | B


-- INIT


init : (Model, Cmd Msg)
init =
    (model, Cmd.none)


-- UPDATE


type Msg
    = Pick AnsweringQuestionsModel Choice
    | Start
    | StartOver (List AnsweredQuestion)
    | FetchChoices
    | FetchSucceed UnansweredQuestion
    | FetchFail Http.Error

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Start ->
            ( { model | answeringState = Loading }, getChoices)

        Pick answeringQuestionsModel choice ->
            let
                answeredQuestions =
                  choice
                      |> answerQuestion answeringQuestionsModel.currentQuestion
                      |> ListExtra.singleton
                      |> List.append answeringQuestionsModel.answeredQuestions

                maybeCurrentAndUnanswered =
                      ListExtra.uncons
                          answeringQuestionsModel.unansweredQuestions

                answeringState =
                  case ( nextQuestion answeredQuestions maybeCurrentAndUnanswered ) of
                      NoQuestionsLeft ->
                          Finished answeredQuestions

                      StepSuccess answeringQuestionModel  ->
                          AnsweringQuestions answeringQuestionModel

            in
                ({model | answeringState = answeringState}, Cmd.none  )

        StartOver answeredQuestions ->
            let
                maybeCurrentAndUnanswered =
                    answeredQuestions
                      |> List.map resetQuestion
                      |> ListExtra.uncons

                answeringState =
                    case ( nextQuestion [] maybeCurrentAndUnanswered ) of
                        NoQuestionsLeft ->
                          NoQuestionsProvided

                        StepSuccess answeringQuestionModel ->
                          AnsweringQuestions answeringQuestionModel
            in
              ({ model | answeringState = answeringState }, Cmd.none)

        FetchChoices ->
          (model, Cmd.none)
        
        FetchSucceed choices ->
            let
                questions = 
                    choices 
                        |> List.foldr toQuestions []
                        |> Debug.log "After fold"
                        -- Sort by questionNumber
                     
                maybeCurrentAndUnanswered = ListExtra.uncons questions
                answeringState =
                  case ( nextQuestion [] maybeCurrentAndUnanswered ) of
                      NoQuestionsLeft ->
                          NoQuestionsProvided
                      
                      StepSuccess answeringQuestionModel  ->
                          AnsweringQuestions answeringQuestionModel
          
            in
                ( { model | answeringState = answeringState }, Cmd.none )

        FetchFail error ->
              ({ model | answeringState = LoadError error }, Cmd.none)


type StepResult
    = StepSuccess AnsweringQuestionsModel
    | NoQuestionsLeft


toQuestions : Choice -> List UnansweredQuestion -> List UnansweredQuestion
toQuestions choice questions =
    (toQuestion choice) :: questions


toQuestion : Choice -> UnansweredQuestion
toQuestion choice =
    [ choice ]


nextQuestion : (List AnsweredQuestion) -> Maybe (UnansweredQuestion, List UnansweredQuestion) -> StepResult
nextQuestion answeredQuestions currentAndUnansweredQuestions =
    case currentAndUnansweredQuestions of
        Nothing ->
            NoQuestionsLeft

        Just (currentQuestion, unansweredQuestions) ->
            StepSuccess
              ( AnsweringQuestionsModel
                  answeredQuestions
                  currentQuestion
                  unansweredQuestions
              )


answerQuestion : UnansweredQuestion -> Choice -> AnsweredQuestion
answerQuestion question pickedChoice =
    let
        (before, after) =
            question
              |> ListExtra.remove pickedChoice
              |> ListExtra.break ( \x -> x == pickedChoice )
    in
        AnsweredQuestion before pickedChoice after


resetQuestion : AnsweredQuestion -> UnansweredQuestion
resetQuestion answered =
    List.concat
        [ answered.beforeChosen
        , [ answered.chosen ]
        , answered.afterChosen
        ]


-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


-- HTTP


url : String
url =
    "http://api.thehappinesstrapsidekick.com/api/choices"

getChoices : Cmd Msg
getChoices =
    Task.perform FetchFail FetchSucceed (Http.get choicesDecoder url)


choicesDecoder : Decode.Decoder (List Choice)
choicesDecoder =
    Decode.list choiceDecoder


choiceDecoder : Decode.Decoder Choice
choiceDecoder =
    decode Choice
        |> Pipeline.required "choiceId" Decode.int
        |> Pipeline.required "questionNumber" Decode.int
        |> Pipeline.required "description" Decode.string
        |> Pipeline.required "choiceType" choiceTypeDecoder


choiceTypeDecoder : Decode.Decoder ChoiceType
choiceTypeDecoder =
    let
        decodeToType string =
            case string of
                "A" ->
                    Result.Ok A
                "B" ->
                    Result.Ok B
                _ ->
                    Result.Err ("Not valid pattern for decoder to ChoiceType: " ++ (toString string))
    in
       Decode.customDecoder Decode.string decodeToType


-- VIEW

grid : List (Html Msg) -> Html Msg
grid children =
    div [ class "grid" ] children

columnHeavy : List (Html Msg) -> Html Msg
columnHeavy children =
    div [ class "column-heavy" ] children

pageTitle : Html Msg
pageTitle =
    h1
        [ class "pageTitle" ]
        [ text "THE HAPPENIESS TRAP"
        , strong [] [ text "SIDEKICK" ]
        ]


wrapper :  String -> List (Html Msg) -> Html Msg
wrapper classname children  =
    section [ class <| "wrapper " ++ classname ] children


bookImage : Html Msg
bookImage =
    img [ src "./book-image.png" ] []


view : Model -> Html Msg
view model =
    case model.answeringState of
        ReadyToStart  ->
            wrapper
                "readyToStart"
                [ div
                    [ class "readyToStart-inner" ]
                    [ div [ class "welcome-message"]
                      [ pageTitle
                      , p [] [ text model.description ]
                      , button 
                          [ onClick Start ] 
                          [ text "CLICK HERE to start" ]
                      ]
                    , div
                        [ class "image-container" ]
                        [ bookImage
                        ]
                    ]
                ]

        Loading ->
            wrapper
                "loading"
                [ div
                    []
                    [ text "Loading"
                    ]
                ]

        LoadError error ->
            let 
                message = 
                    case error of
                        Http.Timeout ->
                            "timeout"
                        Http.NetworkError ->
                            "NetworkError"
                        Http.UnexpectedPayload msg ->
                            "UnexpectedPayload " ++ msg
                        Http.BadResponse int string ->
                            "BadResponse " ++ string
            in
                wrapper
                    "load-error"
                    [ div
                        []
                        [ text message
                        ]
                    ]

        AnsweringQuestions answeringQuestionsModel ->
            div
              []
              [ pageHeader
              , wrapper
                  "answeringQuestions"
                  [ viewQuestionsModel answeringQuestionsModel
                  ]
              ]

        Finished answeredQuestions ->
            div
                []
                [ pageHeader
                , wrapper
                    "finished"
                    [ viewFinished answeredQuestions
                    ]
                ]

        NoQuestionsProvided ->
            wrapper
                "noQuestionsProvided"
                [ p [] [ text "No questions provided" ]
                ]


viewQuestionsModel : AnsweringQuestionsModel -> Html Msg
viewQuestionsModel answeringQuestionsModel =
    let
        elements =
            answeringQuestionsModel.currentQuestion
                |> List.map (viewChoice answeringQuestionsModel)
                |> List.intersperse orElement
    in
        div
            [  ]
            ( h2 [] [ text "Which best describe how you feel?" ]
              :: elements
            )


orElement : Html Msg
orElement =
    div
        [ class "or-divider-container" ]
        [ div [ class "line" ] []
        , p [] [ text "or" ]
        , div [ class "line" ] []
        ]


viewChoice : AnsweringQuestionsModel -> Choice -> Html Msg
viewChoice answeringQuestionsModel choice =
    button
        [ onClick ( Pick answeringQuestionsModel choice )
        , class "choice"
        ]
        [ circleOutline
        , div [ class "content" ] [ text choice.description ]
        ]


circleOutline : Html Msg
circleOutline =
  div
      [ class "circle-container" ]
      [ div [ class "circle" ] []
      ]


viewFinished : List AnsweredQuestion -> Html Msg
viewFinished answeredQuestions =
    let
        (numA, numB) = results answeredQuestions
    in
        div
            [ class "results" ]
            [ h2 [] [ text "Results " ]
            , p [] (scoreDescription "A" numA)
            , p [] (scoreDescription "B" numB)
            , button
               [ onClick ( StartOver answeredQuestions ) ]
               [ text "CLICK HERE to start again"]
            ]

scoreDescription : String -> Int -> List (Html Msg)
scoreDescription name num =
    let
         time_s_ =
             if num == 1 then
                "time"
             else
                "times"
    in
       [ strong [] [ text name ]
       , text " was picked "
       , strong [] [ text (toString num) ]
       , text (" " ++ time_s_)
       ]


results : List AnsweredQuestion -> (Int, Int)
results answeredQuestions = List.foldl accumulateResult (0, 0) answeredQuestions


accumulateResult : AnsweredQuestion -> (Int, Int) -> (Int, Int)
accumulateResult question acc =
    let
        (numA, numB) = acc
    in
        case question.chosen.choiceType of
            A ->
                (numA+1, numB)

            B ->
                (numA, numB+1)


pageHeader : Html Msg
pageHeader =
    header
      [ class "pageHeader" ]
      [ h1
        [ ]
        [ text "THE HAPPINESS TRAP "
        , strong [] [ text "SIDEKICK" ]
        ]
      ]


-- DATA


questionnaireData : Model
questionnaireData =
    { title = "Control of Thoughts and Feelings Questionnaire"
    , description = "This questionnaire has been adapted from similar ones developed by Steven Hayes, Frank Bond, and others. Please pick the statement that most accurately fits how you feel. The answer you choose doesn’t have to be absolutely 100 percent true for you all the time; just pick the answer which seems to be more representative of your general attitude."
    , reference = "https://www.thehappinesstrap.com/upimages/Control_of_Thoughts_and_Feelings_Questionnaire.pdf"
    , copyright = "© Russ Harris 2008"
    , answeringState = ReadyToStart
    }
{--
questionsData : List UnansweredQuestion
questionsData =
    List.map
        initQuestion
        [ ( "I must have good control of my feelings in order to be successful in life."
          , "It is unnecessary for me to control my feelings in order to be successful in life."
          )
        , ( "Anxiety is bad."
          , "Anxiety is neither good nor bad. It is merely an uncomfortable feeling."
          )
        , ( "Negative thoughts and feelings will harm you if you don’t control or get rid of them."
          , "Negative thoughts and feelings won’t harm you even if they feel unpleasant."
          )
        , ( "I’m afraid of some of my strong feelings."
          , "I’m not afraid of any feelings, no matter how strong."
          )
        , ( "In order for me to do something important, I have to get rid of all my doubts."
          , "I can do something important, even when doubts are present."
          )
        , ( "When negative thoughts and feelings arise, it’s important to reduce or get rid of them as quickly as possible."
          , "Trying to reduce or get rid of negative thoughts and feelings frequently causes problems. If I simply allow them to be, then they will change as a natural part of living."
          )
        , ( "The best method of managing negative thoughts and feelings is to analyze them; then utilize that knowledge to get rid of them."
          , "The best method of managing negative thoughts and feelings is to acknowledge their presence and let them be, without having to analyze or judge them."
          )
        , ( "I will become “happy” and “healthy” by improving my ability to avoid, reduce, or get rid of negative thoughts and feelings."
          , "I will become “happy” and “healthy” by allowing negative thoughts and feelings to come and go of their own accord and learning to live effectively when they are present"
          )
        , ( "If I can’t suppress or get rid of a negative emotional reaction, it’s a sign of personal failure or weakness."
          , "The need to control or get rid of a negative emotional reaction is a problem in itself."
          )
        , ( "Having negative thoughts and feelings is an indication that I’m psychologically unhealthy or I’ve got problems."
          , "Having negative thoughts and feelings means I’m a normal human being."
          )
        , ( "People who are in control of their lives can generally control how they feel."
          , "People who are in control of their lives do not need to control their feelings."
          )
        , ( "It is not okay to feel anxious and I try hard to avoid it."
          , "I don’t like anxiety, but it’s okay to feel it."
          )
        , ( "Negative thoughts and feelings are a sign that there is something wrong with my life."
          , "Negative thoughts and feelings are an inevitable part of life for everyone."
          )
        , ( "I have to feel good before I can do something that’s important and challenging."
          , "I can do something that’s important and challenging even if I’m feeling anxious or depressed."
          )
        , ("I try to suppress thoughts and feelings that I don’t like by just not thinking about them."
          , "I don’t try to suppress thoughts and feelings that I don’t like. I just let them come and go of their own accord."
          )
        ]
--}
-- initQuestion : (String, String) -> UnansweredQuestion
-- initQuestion (aText, bText) =
--     [ Choice aText A, Choice bText B ]

